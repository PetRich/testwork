<?php

require __DIR__.'/vendor/autoload.php';
$config = require __DIR__ . '/src/config/config.php';

$container = new Petrich\Tree\DI\Container($config['container']);

$tree = $container->get('Tree');
$maxWeight = 10;

echo '<pre>';
print_r($tree->processTree($config['tree'], $maxWeight));