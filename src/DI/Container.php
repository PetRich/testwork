<?php

namespace Petrich\Tree\DI;

/**
 * Class Container.
 *
 * Example:
 * Let's register the Tree class in the container so that we can use it later.
 * $container->set('Tree');
 *
 * How to get class from container.
 * $tree = $container->get('Tree');
 */
class Container
{
    /**
     * List of the associates an abstraction with a specific implementation.
     *
     * @var array
     */
    protected array $instances = [];

    /**
     * Container constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        foreach ($config as $alias => $specific)
        {
            $this->set($alias, $specific);
        }
    }

    /**
     * Associates an abstraction with a specific implementation.
     *
     * @param string $abstract
     * @param string|null $concrete
     */
    public function set(string $abstract, $concrete = NULL): void
    {
        if ($concrete === NULL) {
            $concrete = $abstract;
        }
        $this->instances[$abstract] = $concrete;
    }

    /**
     * Gets instance by name.
     *
     * @param string $abstract
     * @param array $parameters
     *
     * @return mixed|null|object
     * @throws \Exception
     */
    public function get(string $abstract,array $parameters = []): ?object
    {
        // if we don't have it, just register it
        if (!isset($this->instances[$abstract])) {
            $this->set($abstract);
        }
        return $this->resolve($this->instances[$abstract], $parameters);
    }

    /**
     * Resolves single.
     *
     * @param $concrete
     * @param array $parameters
     * @return object|null
     * @throws \ReflectionException
     */
    public function resolve($concrete, array $parameters): ?object
    {
        if ($concrete instanceof \Closure) {
            return $concrete($this, $parameters);
        }
        $reflector = new \ReflectionClass($concrete);
        // check if class is instantiable
        if (!$reflector->isInstantiable()) {
            throw new \Exception("Class {$concrete} is not instantiable");
        }
        // get class constructor
        $constructor = $reflector->getConstructor();
        if (is_null($constructor)) {
            // get new instance from class
            return $reflector->newInstance();
        }
        // get constructor params
        $parameters   = $constructor->getParameters();
        $dependencies = $this->getDependencies($parameters);
        // get new instance with dependencies resolved
        return $reflector->newInstanceArgs($dependencies);
    }

    /**
     * Gets all dependencies resolved.
     *
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function getDependencies(array $parameters): array
    {
        $dependencies = [];
        foreach ($parameters as $parameter) {
            // get the type hinted class
            $dependency = $parameter->getClass();
            if ($dependency === NULL) {
                // check if default value for a parameter is available
                if ($parameter->isDefaultValueAvailable()) {
                    // get default value of parameter
                    $dependencies[] = $parameter->getDefaultValue();
                } else {
                    throw new \Exception("Can not resolve class dependency {$parameter->name}");
                }
            } else {
                // get dependency resolved
                $dependencies[] = $this->get($dependency->name);
            }
        }
        return $dependencies;
    }
}