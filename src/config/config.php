<?php
return [
    'container' => [
        'Tree' => 'Petrich\Tree\Tree',
        'Petrich\Tree\SortingAlgorithms\SortingContractInterface' => '\Petrich\Tree\SortingAlgorithms\QuickSort'
    ],
    'tree' => [
        [
            'leaves' => [1,2,3],
            'nodes'  => [
                [
                    'leaves' => [6,2,9],
                    'nodes'  => [
                        [
                            'leaves' => [4,1,11,1],
                            'nodes'  => [
                                [
                                    'leaves' => [1,7,8],
                                    'nodes'  => [
                                        [
                                            'leaves' => [15,23,3,2,1],
                                            'nodes'  => NULL
                                        ],
                                    ]
                                ],
                                [
                                    'leaves' => [5,6,7],
                                    'nodes'  => [
                                        [
                                            'leaves' => [1,3,8,20,25],
                                            'nodes'  => NULL
                                        ],
                                    ]
                                ],
                            ]
                        ],
                        [
                            'leaves' => [13,9,75,24,66],
                            'nodes'  => [
                                [
                                    'leaves' => [55,44,22,1],
                                    'nodes'  => NULL
                                ],
                                [
                                    'leaves' => [2,3,1],
                                    'nodes'  => NULL
                                ],
                            ]
                        ],
                    ]
                ],
                [
                    'leaves' => [15,6,2],
                    'nodes'  => NULL
                ],
            ]
        ]
    ]
];