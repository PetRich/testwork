<?php


namespace Petrich\Tree\SortingAlgorithms;

/**
 * Class CombSort
 */
class CombSort implements SortingContractInterface
{
    /**
     * @param array $array
     * @return array
     */
    public function sort(array $array): array
    {
        $gap = count($array);
        $swap = true;
        while ($gap > 1 || $swap){
            if($gap > 1) $gap /= 1.25;

            $swap = false;
            $i = 0;
            while($i+$gap < count($array)){
                if($array[$i] > $array[$i+$gap]){
                    list($array[$i], $array[$i+$gap]) = [$array[$i+$gap], $array[$i]];
                    $swap = true;
                }
                $i++;
            }
        }
        return $array;
    }
}