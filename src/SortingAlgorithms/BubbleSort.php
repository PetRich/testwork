<?php


namespace Petrich\Tree\SortingAlgorithms;

/**
 * Class BubbleSort
 */
class BubbleSort implements SortingContractInterface
{
    /**
     * @param array $array
     * @return array
     */
    public function sort(array $array): array
    {
        do {
            $swapped = false;
            for ($i = 0; $i < count($array) - 1; ++$i) {
                if ($array[$i] > $array[$i + 1]) {
                    list($array[$i + 1], $array[$i]) =
                        array($array[$i], $array[$i + 1]);
                    $swapped = true;
                }
            }
        } while ($swapped);

        return $array;
    }
}
