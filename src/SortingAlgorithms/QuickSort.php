<?php


namespace Petrich\Tree\SortingAlgorithms;

/**
 * Class QuickSort
 */
class QuickSort implements SortingContractInterface
{
    /**
     * @param array $array
     * @return array
     */
    public function sort(array $array): array
    {
        $left = $right = [];
        if(count($array) < 2){
            return $array;
        }
        $pivot = array_shift($array);
        foreach($array as $value){
            if($value <= $pivot){
                $left[] = $value;
            }elseif ($value > $pivot){
                $right[] = $value;
            }
        }

        return array_merge($this->sort($left), [$pivot], $this->sort($right));
    }
}