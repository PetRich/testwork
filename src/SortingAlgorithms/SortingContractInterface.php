<?php


namespace Petrich\Tree\SortingAlgorithms;

/**
 * Interface SortingContract
 */
interface SortingContractInterface
{
    /**
     * Realizes sorting algorithm.
     *
     * @param array $array
     * @return array
     */
    public function sort(array $array): array;
}