<?php


namespace Petrich\Tree;

use Petrich\Tree\SortingAlgorithms\SortingContractInterface;

/**
 * Class Tree.
 */
class Tree
{
    /**
     * @var SortingContractInterface
     */
    private SortingContractInterface $algorithm;

    /**
     * @var int
     */
    private int $maxWeight = 0;

    /**
     * @var array List of the free leaves.
     */
    private array $freeLeaves = [];

    const NODE = 'nodes';
    const LEAF = 'leaves';

    /**
     * Tree constructor.
     * @param SortingContractInterface $algorithm
     */
    public function __construct(SortingContractInterface $algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * Processes the all tree.
     *
     * @param array $tree
     * @param int $maxWeight
     * @return array
     */
    public function processTree(array $tree, int $maxWeight): array
    {
        $this->maxWeight = $maxWeight;

        return $this->parseNode($tree);
    }

    /**
     * Processes the specific node.
     *
     * @param array $tree
     * @return array
     */
    private function parseNode(array $tree): array
    {
        foreach ($tree as $key => $value) {
            $tree[$key][self::LEAF] = $this->removeLeaves($tree[$key][self::LEAF]);
            if ($tree[$key][self::NODE])
                $tree[$key][self::NODE] = $this->parseNode($tree[$key][self::NODE]);
        }

        return $tree;
    }

    /**
     * Removes leaves.
     *
     * @param array $leaves
     * @return array
     */
    private function removeLeaves(array $leaves): array
    {
        $leaves = $this->algorithm->sort(array_merge($this->freeLeaves, $leaves));
        $totalWeight = array_shift($leaves);

        if ($totalWeight > $this->maxWeight) {
            $this->freeLeaves = $leaves;
            return [];
        }

        $newLeaves = [$totalWeight];

        for ($i = 0; $i < count($leaves); $i++) {
            if($totalWeight + $leaves[$i] <= $this->maxWeight) {
                $newLeaves[] = $leaves[$i];
                $totalWeight += $leaves[$i];
            }
        }

        return $newLeaves;
    }
}