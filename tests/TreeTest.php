<?php


namespace Petrich\Test;

use Petrich\Tree\DI\Container;
use PHPUnit\Framework\TestCase;

class TreeTest extends TestCase
{
    /**
     * @var int
     */
    private int $maxWeight = 5;

    /**
     * @var array|array[]
     */
    private array $configTree =[
        [
            'leaves' => [1,2,3],
            'nodes'  => [
                [
                    'leaves' => [6,2,9],
                    'nodes'  => [
                        [
                            'leaves' => [4,1,11,1],
                            'nodes'  => null
                        ],
                    ]
                ],
            ]
        ]
    ];

    /**+
     * @var array|array[]
     */
    private array $expectedArray = [
        [
            'leaves' => [1,2],
            'nodes'  => [
                [
                    'leaves' => [2],
                    'nodes'  => [
                        [
                            'leaves' => [1,1],
                            'nodes'  => null
                        ],
                    ]
                ],
            ]
        ]
    ];

    /**
     * @var \Petrich\Tree\Tree
     */
    private $tree = null;

    protected function setUp(): void
    {
        $config = require __DIR__ . '/../src/config/config.php';
        $container = new Container($config['container']);
        $this->tree = $container->get('Tree');
    }

    /**
     * Tested data from method {@link \Petrich\Tree\Tree::processTree} must bu equals with {@see \Petrich\Tree\TreeTest::$expectedArray}.
     */
    function testTrue()
    {
        $sortTree = $this->tree->processTree($this->configTree, $this->maxWeight);

        $this->assertIsArray($sortTree);
        $this->assertEquals($this->expectedArray, $sortTree);
    }
}